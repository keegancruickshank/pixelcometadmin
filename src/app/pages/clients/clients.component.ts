import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../_services/navigation.service';
import { ModalService } from '../../_services/modal.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  private user: Object = {};
  public users = null;

  constructor(private _navigation: NavigationService, private _modal: ModalService) {
    _navigation.setBreadcrumb("Clients", true)
  }

  ngOnInit() {
  }

  addUser(id){
    this._modal.open(id);
    console.log(this.user)
  }

  closeAddUser(id: string) {
        this._modal.close(id);
  }

  chooseFile() {
      document.getElementById("profileImageInput").click();
  }

  imageUploaded() {
      let fileElement = <HTMLInputElement>document.getElementById("profileImageInput");
      let fileInput = fileElement.files[0];
      var fr = new FileReader();
      fr.onload = function () {
        let imageElement = <HTMLImageElement>document.getElementById("imageInput")
        imageElement.src = <string>fr.result;
      }
      fr.readAsDataURL(fileInput);
  }

}
