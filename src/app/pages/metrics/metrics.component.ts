import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../_services/navigation.service';

@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.scss']
})
export class MetricsComponent implements OnInit {

  constructor(private _navigation: NavigationService) {
    _navigation.setBreadcrumb("Metrics", true)
  }

  ngOnInit() {
  }

}
