import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../_services/navigation.service';
import { NotificationService } from '../../_services/notification.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private acivatedWidget = "widget-1";

  constructor(private _navigation: NavigationService, private _notification: NotificationService) {
    _navigation.setBreadcrumb("Home", true)
  }

  ngOnInit() {
  }

  activatedCheckStyles(name){
    if(name == this.acivatedWidget){
      return "widget active"
    } else {
      return "widget"
    }
  }

  activate(name){
    this.acivatedWidget = name
  }

  editNotification() {
    let el = <HTMLInputElement>document.getElementById("notification-change")
    let value = el.value
    this._notification.setCount(value)
  }

}
