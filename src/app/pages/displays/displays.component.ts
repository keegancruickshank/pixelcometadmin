import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../_services/navigation.service';
import { ModalService } from '../../_services/modal.service';

@Component({
  selector: 'app-displays',
  templateUrl: './displays.component.html',
  styleUrls: ['./displays.component.scss']
})
export class DisplaysComponent implements OnInit {

  public displays = null;
  private display: Object = {};

  constructor(private _navigation: NavigationService, private _modal: ModalService) {
    _navigation.setBreadcrumb("Displays", true)
  }

  addDisplay(id){
    this._modal.open(id);
    console.log(this.display)
  }

  ngOnInit() {
  }

  closeAddDisplay(id: string) {
        this._modal.close(id);
  }
}
