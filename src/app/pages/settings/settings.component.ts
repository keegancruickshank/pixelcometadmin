import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../_services/navigation.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(private _navigation: NavigationService) {
    _navigation.setBreadcrumb("Settings", true)
  }

  ngOnInit() {
  }

}
