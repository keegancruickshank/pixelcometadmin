import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../../_services/navigation.service';
import { UserService } from '../../_services/user.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(private _navigation: NavigationService, private _user: UserService) {
    _navigation.setBreadcrumb("My Account", true)
  }

  ngOnInit() {
  }

  logout() {
    this._user.logout()
  }

}
