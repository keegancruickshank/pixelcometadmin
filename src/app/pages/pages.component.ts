import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { NavigationService } from '../_services/navigation.service';
import { NotificationService } from '../_services/notification.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  public breadcrumb: Observable<string> = this._navigation.breadcrumb;
  public notificationCount: Observable<number> = this._notification.count;
  private _notificationClass:BehaviorSubject<string> = new BehaviorSubject("far fa-bell")
  public notificationClass: Observable<string> = this._notificationClass.asObservable();

  constructor(private _navigation: NavigationService, private _notification:NotificationService) {
  }

  ngOnInit() {
    let that = this
    this.notificationCount.subscribe(count => {
      if (count == 0) {
        that._notificationClass.next("far fa-bell")
        document.getElementById("badge").className = "menu-icon badge hide-after"
      } else {
        that._notificationClass.next("far fa-bell animated swing infinite");
        document.getElementById("badge").className = "menu-icon badge"
      }
    })
  }

  loadedImage() {
    document.getElementById("profile-image").className = "profile-image animated fadeIn"
  }

}
