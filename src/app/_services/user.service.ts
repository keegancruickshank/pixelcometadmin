import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { auth } from 'firebase';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: User
  private userObject: Observable<any>
  private _subscribeList: Array<any> = []
  private userLoggedIn = new BehaviorSubject<boolean>(false)
  public status: Observable<boolean> = this.userLoggedIn.asObservable();


  constructor(public afAuth: AngularFireAuth, public db: AngularFireDatabase) {}

  login(email: string, password: string): Promise<boolean> {
    let that = this
    return new Promise(function(resolve, reject) {
      that.afAuth.auth.signInWithEmailAndPassword(email, password).then(response => {
        that.userObject = that.db.object('pcga/' + response.user.uid).valueChanges()
        let subscription = that.userObject.subscribe(data => {
          that.user = new User(data)
          resolve(true)
        })
        that._subscribeList.push(subscription)
      }).catch(error => {
        that.user = null
        reject(error)
      })
    })
  }

  loadUserPriorBootstrap(): Promise<boolean> {
    let that = this
    return new Promise(function(resolve, reject) {
      that.afAuth.auth.onAuthStateChanged(function(user) {
        if (user) {
          that.userLoggedIn.next(true)
          let query = that.db.object('pcga/' + user.uid)
          that.userObject = query.valueChanges()
          let subscription = that.userObject.subscribe(data => {
            if (data != null) {
              that.user = new User(data)
              resolve(true)
            } else {
              that.logout()
            }
          })
          that._subscribeList.push(subscription)
        } else {
          that.user = null
          that.userLoggedIn.next(false)
          resolve(false)
        }
      })
    })
  }

  loggedInWatcher() {
    return this.userLoggedIn.asObservable()
  }

  logout() {
    this.removeSubscriptions()
    this.afAuth.auth.signOut()
  }

  removeSubscriptions() {
    for(var subscription in this._subscribeList) {
      this._subscribeList[subscription].unsubscribe()
      //this._subscribeList.splice(subscription, 1);
    }
  }

  getPrimaryContactFullName() {
    return this.user.primaryContactFirstName + " " + this.user.primaryContactLastName
  }

  getPrimaryContactFirstName() {
    return this.user.primaryContactFirstName
  }

  getPrimaryContactLastName() {
    return this.user.primaryContactLastName
  }

  getPrimaryContactEmailAddress() {
    return this.user.primaryContactEmail
  }
}

class User {

  displays: object
  clients: object
  uid: string
  reviews: object
  businessName: string
  businessPhone: string
  businessAddress: object
  businessPostalAddress: object
  primaryContactFirstName: string
  primaryContactLastName: string
  primaryContactEmail: string
  primaryContactNumber: string

  constructor(firebaseBusinessGlobalObject) {
    this.reviews = firebaseBusinessGlobalObject.reviews
    this.clients = firebaseBusinessGlobalObject.clients
    this.displays = firebaseBusinessGlobalObject.displays
    this.uid = firebaseBusinessGlobalObject.uid
    this.primaryContactFirstName = firebaseBusinessGlobalObject.primaryContactFirstName
    this.primaryContactLastName = firebaseBusinessGlobalObject.primaryContactLastName
    this.primaryContactEmail = firebaseBusinessGlobalObject.primaryContactEmail
    this.primaryContactNumber = firebaseBusinessGlobalObject.primaryContactNumber
    this.businessName = firebaseBusinessGlobalObject.entity
    this.businessAddress = firebaseBusinessGlobalObject.businessAddress
    this.businessPostalAddress = firebaseBusinessGlobalObject.businessPostalAddress
  }
}
