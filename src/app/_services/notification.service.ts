import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private _count: BehaviorSubject<number> = new BehaviorSubject(0);
  public count: Observable<number> = this._count.asObservable();

  constructor() { }

  setCount(number) {
    this._count.next(number)
  }

  logoutRun() {
    this._count.next(0);
  }
}
