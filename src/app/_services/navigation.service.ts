import { Injectable, HostListener } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  private _breadcrumb: BehaviorSubject<string> = new BehaviorSubject("Home");
  public breadcrumb: Observable<string> = this._breadcrumb.asObservable();
  private _title: BehaviorSubject<string> = new BehaviorSubject("Home");
  public title: Observable<string> = this._title.asObservable();
  public location: string = "";


  constructor() {
    this.title.subscribe(text => {
      document.title = text;
    })
  }

  setTitle(breadcrumbText, focused) {
    if(focused) {
      this._title.next("Pixel Comet | " + breadcrumbText)
      this.location = breadcrumbText
    } else {
      this._title.next(breadcrumbText)
    }
  }

  setBreadcrumb(breadcrumbText, focused){
    this._breadcrumb.next(breadcrumbText);
    this._title.next("Pixel Comet | " + breadcrumbText)
    this.location = breadcrumbText
  }
}
