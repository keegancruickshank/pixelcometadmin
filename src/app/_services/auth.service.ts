import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private _user: UserService, private _notification: NotificationService) {
    let that = this;
    _user.status.subscribe(status => {
      if (status == false) {
        that.logoutRun()
        router.navigate(['/login']);
      } else {
        router.navigate(['/pages/home']);
      }
    })
  }

  logoutRun() {
    this._notification.logoutRun()
  }
}
