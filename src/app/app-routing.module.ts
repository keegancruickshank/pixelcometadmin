import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AuthComponent } from './auth/auth.component';
import { PagesComponent } from './pages/pages.component';
import { HomeComponent } from './pages/home/home.component';
import { DisplaysComponent } from './pages/displays/displays.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { MetricsComponent } from './pages/metrics/metrics.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { AccountComponent } from './pages/account/account.component';

const routes: Routes = [
  { path: 'authorise', component: AuthComponent },
  { path: 'login', component: LoginComponent },
  { path: 'pages', component: PagesComponent,
      children: [
        { path: '', redirectTo: 'home', pathMatch: 'full'},
        { path: 'home', component:  HomeComponent },
        { path: 'displays', component:  DisplaysComponent },
        { path: 'clients', component:  ClientsComponent },
        { path: 'metrics', component:  MetricsComponent },
        { path: 'settings', component:  SettingsComponent },
        { path: 'account', component:  AccountComponent }
      ]
  },
  { path: '',
    redirectTo: 'authorise',
    pathMatch: 'full'
  },
  { path: '**', component: AuthComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
