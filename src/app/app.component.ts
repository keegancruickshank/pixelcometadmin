import { Component, HostListener } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { NavigationService } from './_services/navigation.service';
import { NotificationService } from './_services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Pixel Comet';
  private away = false;
  private notificationNumber = 0;

  constructor(private _auth: AuthService, private _navigation: NavigationService, private _notification: NotificationService){
    let that = this;
    that._notification.count.subscribe(number => {
      that.notificationNumber = number
    })
  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: any): void {
    this.away = false
    this._navigation.setTitle(this._navigation.location, true)
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: any): void {
    this.away = true
    this.runAwayScript()
  }

  runAwayScript() {
    let that = this
    if(this.away && this.notificationNumber != 0){
      that._navigation.setTitle(that.notificationNumber + (that.notificationNumber == 1 ? " New Notification" : " New Notifications") + " 🔔", false)
      setTimeout(function() {
        that._navigation.setTitle(that._navigation.location, true)
        setTimeout(function() {
          that.runAwayScript()
        }, 2000)
      }, 2000)
    }
  }
}
