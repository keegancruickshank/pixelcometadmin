import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER  } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { UserService } from './_services/user.service';
import { NavigationService } from './_services/navigation.service';
import { AuthService } from './_services/auth.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { DisplaysComponent } from './pages/displays/displays.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { MetricsComponent } from './pages/metrics/metrics.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { AccountComponent } from './pages/account/account.component';
import { JwModalComponent } from './_directives/jw-modal/jw-modal.component';


export function configFactory(loader: UserService) {
  return  () => loader.loadUserPriorBootstrap();
}

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    AuthComponent,
    LoginComponent,
    HomeComponent,
    DisplaysComponent,
    ClientsComponent,
    MetricsComponent,
    SettingsComponent,
    AccountComponent,
    JwModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  providers: [
    AuthService,
    NavigationService,
    UserService,
      {
        provide: APP_INITIALIZER,
        useFactory: configFactory,
        deps: [UserService],
        multi: true
      }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
