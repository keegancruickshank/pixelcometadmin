import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { UserService } from '../../_services/user.service';
import { NavigationService } from '../../_services/navigation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: LoginUser = {
    email: "",
    password: ""
  }

  private errorMessage:BehaviorSubject<String> = new BehaviorSubject("Error");
  public error:Observable<String> = this.errorMessage.asObservable();

  user: UserService

  constructor(private _user: UserService, private router: Router, private _navigation: NavigationService) {
    this.user = _user
    _navigation.setTitle("Login", true)
  }

  ngOnInit() {
  }

  submit() {
    let that = this;
    let button = <HTMLButtonElement>document.getElementById("submit-button")
    button.disabled = true
    button.className = "loading"
    this.user.login(this.model.email, this.model.password).then(user => {
      this.router.navigate(['pages']);
    }).catch(error => {
      document.getElementById("main-login").style.display = "none";
      document.getElementById("error-login").style.display = "block";
      switch(error.code) {
        case "auth/user-not-found":
          that.errorMessage.next("No user account with that email found")
          break
        case "auth/wrong-password":
          that.errorMessage.next("Incorrect password, try again")
          break
        case "auth/user-disabled":
          that.errorMessage.next("User account disabled, please contact your account manager")
          break
        case "auth/invalid-email":
          that.errorMessage.next("Invalid email address")
          break
        case "auth/argument-error":
          that.errorMessage.next("Please fill both Email and Password")
          break
        default:
          that.errorMessage.next("Please check your credentials and try again.")
      }
    })
  }

  returnToLogin() {
    document.getElementById("main-login").style.display = "block";
    document.getElementById("error-login").style.display = "none";
    let button = <HTMLButtonElement>document.getElementById("submit-button")
    button.disabled = false
    button.className = ""
  }

  /*onSubmit() {
    let that = this
    that.displayError = false
    that.loading = true
    this.user.login(this.model.email, this.model.password).then(user => {
      that.loading = false
      this.router.navigate(['pages']);
    }).catch(error => {
      that.displayError = true
      switch(error.code) {
        case "auth/user-not-found":
          that.errorMessage = "No user account with that email found"
          break
        case "auth/wrong-password":
          that.errorMessage = "Incorrect password, try again"
          break
        case "auth/user-disabled":
          that.errorMessage = "User account disabled, please contact your account manager"
          break
        case "auth/invalid-email":
          that.errorMessage = "Invalid email address"
          break
        case "auth/argument-error":
          that.errorMessage = "Please fill both Email and Password"
          break
        default:
          that.errorMessage = "Please check your credentials and try again."
      }
      that.loading = false
    })
  }*/
}

class LoginUser {

  email: string
  password: string

  constructor(email, password) {
    this.email = email
    this.password = password
  }
}
